const express = require('express'); // Backend
const app = express(); //backend
const server = require('http').Server(app);
const io = require('socket.io').listen(server);
const Map = require('./public/js/Map');
const COLLECTIBLES_AMOUNT = 6;
const EAGLES_AMOUNT = 1;

let score = {blue: 0, red:0};
let players = {};
let mapData = new Map(13, 10, 44);

let collectibles = [];
populateCollectibles();

let eagles = [];
populateEagles();

app.use(express.static(__dirname + '/public'));

app.get('/', function (req, res) {
  res.sendFile(__dirname + '/index.html');
});

io.on('connection', function (socket) {
  console.log('a user connected: ', socket.id);
  // create a new player and add it to our players object
  players[socket.id] = {
    rotation: 0,
    position: {
      x: Math.floor(Math.random() * 700) + 50,
      y: Math.floor(Math.random() * 500) + 50
    },
    playerId: socket.id,
    team: (Math.floor(Math.random() * 2) == 0) ? 'red' : 'blue'
  };
  // Send the map struc to the client
  socket.emit('data', mapData);

  // send the players object to the new player
  socket.emit('currentCollectibles', collectibles);
  socket.emit('currentEagles', eagles);
  socket.emit('currentPlayers', players);

  socket.broadcast.emit('newPlayer', players[socket.id]);

  // when a player disconnects, remove them from our players object
  socket.on('disconnect', function () {
    console.log('user disconnected: ', socket.id);
    delete players[socket.id];
    // emit a message to all players to remove this player
    io.emit('disconnect', socket.id);
  });

  // when a player moves, update the player data
 socket.on('playerMovement', function (movementData) {
    players[socket.id].position = movementData.position;
    players[socket.id].rotation = movementData.rotation;
    // emit a message to all players about the player that moved
    socket.broadcast.emit('playerMoved', players[socket.id]);
  });

  socket.on('shot-broadcast', function (movementData) {
    let new_bullet = movementData;
    socket.broadcast.emit('update-bullets', new_bullet);
  });

  socket.on('score-broadcast', function (scoreData) {
    let new_score = scoreData;
    socket.broadcast.emit('update-score', new_score);
  });
  
  socket.on('bonusCollected', function (collectibleId){
    let collectibleIndex = collectibles.findIndex((element)=>{if(element != undefined)return element.id == collectibleId;})
    delete collectibles[collectibleIndex];
    socket.broadcast.emit('collectibleTaken',collectibleIndex)
  });

});

server.listen(8000, function () {
  console.log(`Listening on ${server.address().port}`);
});

function populateCollectibles(){
  let emptyList = mapData.emptyTiles,
      randomEmptyTile,
      randomPower;
  for(let i = 0; i < COLLECTIBLES_AMOUNT; i++){ //poderes aleatorios speed/strong/life
    randomPower = Math.floor(Math.random() * (3));

    do randomEmptyTile = Math.floor(Math.random() * (emptyList.length));
    while(emptyList[randomEmptyTile].collectibleOnIt);
    
    mapData.modifyEmptyList(randomEmptyTile, true);

    emptyTile = emptyList[randomEmptyTile];
    let collectible = {type: ((randomPower == 0) ? 'speed' : (randomPower == 1) ? 'shield' : 'life'), x: (emptyTile.x * 64) + 16, y: (emptyTile.y * 64) + 16, id: i};
    console.log(collectible);
    collectibles.push(collectible);
  }
} 

function populateEagles() {
  let emptyList = mapData.emptyTiles,
      randomEmptyTile;
  for(let i = 0; i < EAGLES_AMOUNT; i++){ // Aguilas 
    do randomEmptyTile = Math.floor(Math.random() * (emptyList.length));
    while(emptyList[randomEmptyTile].collectibleOnIt);
    
    mapData.modifyEmptyList(randomEmptyTile, true);

    emptyTile = emptyList[randomEmptyTile];
    let eagle = {type: 'eagle', x: (emptyTile.x * 64), y: (emptyTile.y * 64), id: i};
    console.log(eagle);
    eagles.push(eagle);
  }
} 
