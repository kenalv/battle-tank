class Map {
    constructor (Width, Height, Wall_Quantity) {
        this.width = Width;
        this.height = Height;
        this.data = this.generateRandomMap(Wall_Quantity);
        this.wallTiles = this.getList(0);
        this.emptyTiles = this.getList(1);
        //this.socketId = socketId;
    }

    generateRandomMap(Wall_Quantity) {
        //  Create some map data dynamically
        //  Map size is Width x Height tiles
        var dataTemp = '';
        var random = '';

        for (var y = 0; y < this.height; y++)
        {
            var temp = 5;
            for (var x = 0; x < this.width; x++)
            {
                random = Math.floor(Math.random() * (2 - 0));
                if (random == 0 && temp != 0)
                    if (Wall_Quantity == 0) random = 1;
                    else
                    {
                        Wall_Quantity--;
                        temp--;
                    }
                dataTemp += random;

                if (x < (this.width - 1)) dataTemp += ',';
            }
            if (y < (this.height - 1)) dataTemp += "\n";
        }
        return dataTemp;
    }

    /*
    *
    *   If type = 1, return the empty map coordenates
    *   If type = 0, return the wall map coordenates
    *
    * */
    getList(type) {
        var temp = [];
        var counter = 0,
            stringCursor,
            tempX = 0;

        for (var y = 0; y < this.height; y++)
        {
            for (var x = 0; x < this.width * 2; x++)
            {
                stringCursor = (y * this.width * 2) + x;

                if (this.data[stringCursor] == ',' || this.data[stringCursor] == '\n')
                    continue;
                else
                {                
                    if (this.data[stringCursor] == type) 
                    {
                        temp[counter] = {x: tempX, y: y, collectibleOnIt: false, positionOnString: stringCursor};
                        counter++;
                    }
                }
                tempX++;
            }
            tempX = 0;
        }
        return temp;
    }

    modifyEmptyList(position, value) {
        this.emptyTiles[position].collectibleOnIt = value;
    }

    toArray() {
        var temp = [];
        var cont = 0;

        for (var y = 0; y < this.height; y++)
            for (var x = 0; x < this.width; x++)
            {
                var idk = (y * this.width) + x;
                if (idk % 2 == 0)
                {
                        temp[cont] = tilePos;
                        cont++;
                }
            }
        return temp;
    }

}

module.exports = Map