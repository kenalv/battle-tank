class TankObject {
    constructor (position, game, socketId, team){
        this.game = game;
        //this.health = 3;
        this.alive = true;
        this.socketId = socketId;
        if(team === 'red'){
            this.tank = game.add.sprite(position.x, position.y, 'tank2', 'tank1');
            this.turret = game.add.sprite(position.x, position.y, 'tank2', 'turret');    
        }else{
            this.tank = game.add.sprite(position.x, position.y, 'tank', 'tank1');
            this.turret = game.add.sprite(position.x, position.y, 'tank', 'turret');
        }
        
        this.tank.anchor.setTo(0.5, 0.5);
        this.turret.anchor.setTo(0.3, 0.5);
        this.tank.team = team;
        this.tank.animations.add('move', ['tank1', 'tank2', 'tank3', 'tank4', 'tank5', 'tank6'], 20, true);
        this.tank.name = "Unnamed";
        this.tank.rotation = 0; 
        game.physics.enable(this.tank, Phaser.Physics.ARCADE);
        this.tank.body.drag.set(0.2);
        this.tank.body.maxVelocity.setTo(400, 400);
        this.tank.body.collideWorldBounds = true;
        
        this.tank.angle = game.rnd.angle();
    }
};