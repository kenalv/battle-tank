// Canvas Window Width Size
let gameWidth = 800;

// Canvas Window Height Size
let gameHeight = 1200;

let game = new Phaser.Game(gameHeight, gameWidth, Phaser.AUTO, 'phaser-example', 
{preload: preload, create: create, update: update, render: render});

//GROUND
let land;

//PLAYER'S
let allies; //ally players group.

//--player's tank
let tank;
let tank2;

//--players turrets
let turret;
let turret2;

//--players tanks shadows
let shadow;
let shadow2;

//--players bullet explotion effetc 
let explosions;

//--players bullet charge 
let bullets;

//--player's movespeed 
let currentSpeed = 0;

//--player's LEFT,RIGHT,DOWN,UP keyboard events
let cursors;

//--player's bullet fire rates
let fire_timer = 0;
const FIRE_RATE = 400;
//SOKECT
let socket;
// TileMap Variable
let map;
// Variable for layer
let layer;

function preload () {
    game.load.atlas('tank', 'assets/tanks.png', 'assets/tanks.json');
    game.load.atlas('tank2', 'assets/tanks.png', 'assets/tanks.json');
    game.load.image('earth', 'assets/earth.png');
    game.load.image('bullet', 'assets/bullet.png');
    game.load.image('wall', 'assets/brickwall.png');
    
}

function create () {
   //get Socket ..
    socket = io();
     //Allies group
    allies = [];

    //  Resize our game world to be a 2000 x 2000 square
    game.world.setBounds(-1000, -1000, 2000, 2000);

    land = game.add.tileSprite(0, 0, gameHeight, gameWidth, 'earth');
    land.fixedToCamera = true;

    //Get all players connected
    socket.on('currentPlayers', function (players) {
        Object.keys(players).forEach(function (id) {
          if (players[id].playerId === socket.id) {
            addMyPlayer(players[id]);
          }else {
            addTeammates(players[id]);
          }
        });
      });

    //Add a new player
    socket.on('newPlayer', function (playerInfo) {
        addTeammates(playerInfo);
    });

    // If the client dc the player is distroyed.
    socket.on('disconnect', function (playerId) {
        allies.forEach(function (allied) {
          if (playerId === allied.tank.playerId) {
              //destroy whole tank.
            allied.tank.destroy();
            allied.turret.destroy();
            allied.shadow.destroy();
          }
        });
    });

    // Send the our positions changes
    socket.on('playerMoved', function (playerInfo) {
        allies.forEach(function (allied) {
          if (playerInfo.playerId === allied.tank.playerId) {
            allied.tank.position =playerInfo.position;
            allied.tank.rotation =playerInfo.rotation;
            allied.turret.position = playerInfo.turret_position;
            allied.turret.rotation = playerInfo.turret_rotation;
            allied.shadow.position = playerInfo.shadow_position;
            allied.shadow.rotation = playerInfo.shadow_rotation;
          }
        });
    });
    cursors = game.input.keyboard.createCursorKeys();
}
 
function update () {
    if(tank){
        if (cursors.left.isDown)
            tank.angle -= 4;
        else if (cursors.right.isDown)
            tank.angle += 4;
        if (cursors.up.isDown)
        {
            //  The speed we'll travel at
            currentSpeed = 300;
        }
        else
        {
            if (currentSpeed > 0)
            {
                currentSpeed -= 4;
            }
        }
        //console.log(tank);
        if (currentSpeed > 0)
        {
            game.physics.arcade.velocityFromRotation(tank.rotation, currentSpeed, tank.body.velocity);
        }
        land.tilePosition.x = -game.camera.x;
        land.tilePosition.y = -game.camera.y;

        //Move the shadow to tank position, to improve movement.
        shadow.position = tank.position;
        shadow.rotation = tank.rotation;
        //Move the turret to tank position, to improve movement.
        turret.position = tank.position;
        turret.rotation = game.physics.arcade.angleToPointer(turret);

        // emit player movement
        let position = tank.position;
        let rotation = tank.rotation;
        let turret_position = turret.position;
        let shadow_position =shadow.position;
        let turret_rotation = turret.rotation;
        let shadow_rotation = shadow.rotation;
        let bullet;

        if (tank.oldPosition && (position !== tank.oldPosition.position || rotation !==tank.oldPosition.rotation || turret_position !== turret.oldPosition.position || shadow_position !== shadow.oldPosition.position )) {
            socket.emit('playerMovement', { position: tank.position, rotation: tank.rotation, turret_position: turret.position, shadow_position: shadow.position, turret_rotation : turret.rotation, shadow_rotation: shadow.rotation});
        }
        // save old position data
        tank.oldPosition = {
            position: tank.position,
            rotation: tank.rotation
        };
        turret.oldPosition ={
            position: turret.position,
            rotation: turret.rotation
        };
        shadow.oldPosition = {
            position: shadow.position,
            rotation: shadow.rotation
        };
       
        //Case if click is down 
        if (game.input.activePointer.isDown)
        {
        //Boom!
            fire(bullet);
        }

    }
}

function render () {
    // game.debug.text('Active Bullets: ' + bullets.countLiving() + ' / ' + bullets.length, 32, 32);
    game.debug.text('Enemies: ' + 0 + ' / ' + 0, 32, 32);
}

function addMyPlayer(playerInfo) {
    console.log("my player event");
    //  The base of our tank
    tank = game.add.sprite(playerInfo.x, playerInfo.y, 'tank', 'tank1');
    tank.anchor.setTo(0.5, 0.5);
    tank.animations.add('move', ['tank1', 'tank2', 'tank3', 'tank4', 'tank5', 'tank6'], 20, true);
    //  This will force it to decelerate and limit its speed
    game.physics.enable(tank, Phaser.Physics.ARCADE);
    tank.body.drag.set(0.2);
    tank.body.maxVelocity.setTo(400, 400);
    tank.body.collideWorldBounds = true;
    //  Finally the turret that we place on-top of the tank body
    turret = game.add.sprite(playerInfo.x, playerInfo.y, 'tank', 'turret');
    turret.anchor.setTo(0.3, 0.5);
    //  A shadow below our tank
    shadow = game.add.sprite(playerInfo.x, playerInfo.y, 'tank', 'shadow');
    shadow.anchor.setTo(0.5, 0.5);
    //  Our bullet group
    bullets = game.add.group();
    bullets.tank_id = tank.playerId;
    bullets.enableBody = true;
    bullets.physicsBodyType = Phaser.Physics.ARCADE;
    bullets.createMultiple(30, 'bullet', 0, false);
    bullets.setAll('anchor.x', 0.5);
    bullets.setAll('anchor.y', 0.5);
    bullets.setAll('outOfBoundsKill', true);
    bullets.setAll('checkWorldBounds', true);
    //Bring Tank and Turret infront of the bullet     
    tank.bringToTop();
    turret.bringToTop();  
    // Camera Follow the tank. 
    game.camera.follow(tank);
    game.camera.deadzone = new Phaser.Rectangle(150, 150, 500, 300);
    game.camera.focusOnXY(playerInfo.x, playerInfo.y);

    allies.push({socketId: socket.id, tank});   //agrego mi tanque a la lista de aliados
    console.log({allies});
}

function addTeammates(playerInfo) {
    console.log("otro player");

function addOtherPlayers(playerInfo) {

    tank2 = game.add.sprite(playerInfo.x, playerInfo.y, 'tank2', 'tank1');
    tank2.anchor.setTo(0.5, 0.5);
    tank2.animations.add('move', ['tank1', 'tank2', 'tank3', 'tank4', 'tank5', 'tank6'], 20, true);
    //  This will force it to decelerate and limit its speed
    game.physics.enable(tank2, Phaser.Physics.ARCADE);
    tank2.body.drag.set(0.2);
    tank2.body.maxVelocity.setTo(400, 400);
    tank2.body.collideWorldBounds = true;
    //  Finally the turret that we place on-top of the tank body
    turret2 = game.add.sprite(playerInfo.x, playerInfo.y, 'tank2', 'turret');
    turret2.anchor.setTo(0.3, 0.5);
    //  A shadow below our tank
    shadow2 = game.add.sprite(playerInfo.x, playerInfo.y, 'tank2', 'shadow');
    shadow2.anchor.setTo(0.5, 0.5);
    tank2.bringToTop();
    turret2.bringToTop();  
    //Add tank2 to allies group
    tank2.playerId = playerInfo.playerId;
    allies.push({tank:tank2,turret:turret2,shadow:shadow2});
}

function fire () {
    if (game.time.now > fire_timer)
    {
        bullet = bullets.getFirstExists(false);
        if(bullet){
            console.log(bullet);
            bullet.reset(turret.x, turret.y);
            bullet.rotation = game.physics.arcade.moveToPointer(bullet,500, game.input.activePointer);
            fire_timer = game.time.now + FIRE_RATE;
        }
    }
}}
