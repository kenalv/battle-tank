class Eagle{
    constructor(position,id,type,game){
        this.type = type;
        this.eagleSprite = game.add.sprite(position.x, position.y, type);
        this.eagleSprite.id = id;
        game.physics.enable(this.eagleSprite, Phaser.Physics.ARCADE);        
    }
}