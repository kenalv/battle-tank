//Dimensiones del contenedor del escenario

let logo;
const FRAME_WIDTH = 832;
const FRAME_HEIGHT = 640;
//Dimensiones del contenedor del escenario
const WORLD_WIDTH = 832;
const WORLD_HEIGHT = 640;

let fire_timer = 0;
const FIRE_RATE = 1000;
let stars =[];

let game = new Phaser.Game(FRAME_WIDTH, FRAME_HEIGHT, Phaser.AUTO, 'phaser-example', 
{preload: preload, create: create, update: update, render: render}); //init

let allies = []; //lista de jugadores aliados (sin mi jugador)
let enemies = [];

let myTeamGroup; //grupo de compañeros (tanques)
let myEnemyGroup;

let collectiblesGroup; //grupo de collecionables (de cualquier tipo)
let collectibles = []; //coleccionables del mundo

let land; //asset de terreno
let socket;

//SCORE
let game_score = {blue: 0, red: 0};
let weapon;
//--players bullet charge 

let game_bullets;
let myPlayer;

let currentSpeed = 0; //control de velocidad
let speedLimit = 100; //velocidad inicial(aumenta con bonus)


//soporte evento teclas LEFT,RIGHT,DOWN,UP 
let cursors;

// Poderes iniciales
let power_1 = 2;    // RED
let power_2 = 2;    // GREEN
let power_3 = 2;    // BLUE

var currentDataString;
let explosion;
let exploded;
let tankHit;
let player_dead;
function preload () {
    game.load.atlas('tank', 'assets/tanks.png', 'assets/tanks.json');
    game.load.atlas('tank2', 'assets/tanks2.png', 'assets/tanks.json');
    game.load.image('earth', 'assets/earth.png');
    game.load.image('bullet', 'assets/bullet.png');
    
    game.load.image('logo', 'assets/logo.png');

    game.load.image('tiles', 'assets/batte-tank-tiles.png');
    game.load.image('speed', 'assets/orb-red.png');
    game.load.image('shield', 'assets/orb-blue.png');
    game.load.image('life', 'assets/orb-green.png');
    game.load.image('star', 'assets/firstaid.png');
    game.load.image('eagle', 'assets/enemyBlack6.png');

    game.load.audio('boom', 'assets/explosion.mp3');
    game.load.audio('exploded', 'assets/explode.wav');
    game.load.audio('tankHit','assets/tankHit.mp3');
    game.load.audio('player_death','assets/player_death.wav');
}

function create () {
    socket = io();

    game.physics.startSystem(Phaser.Physics.ARCADE);
    game.world.setBounds(0, 0, WORLD_WIDTH, WORLD_HEIGHT);
    explosion = game.add.audio('boom');
    exploded = game.add.audio('exploded');
    tankHit = game.add.audio('tankHit');
    //Get all players connected
    socket.on('data', function (dataOnSocket) {
        window.localStorage.setItem('data', dataOnSocket.data);
        window.localStorage.setItem('empty', dataOnSocket.emptyTiles);
    });
    player_death = game.add.audio('player_death'),
    //  Add data to the cache
    game.cache.addTilemap('dynamicMap', null, window.localStorage.getItem('data'), Phaser.Tilemap.CSV);

    land = game.add.tilemap('dynamicMap',64,64);

    land.addTilesetImage('tiles','tiles',64,64,0,0);
    // Wall = 0
    land.setCollision(0);

    land.fixedToCamera = true;

    //  0 is important
    layer = land.createLayer(0);

    //  Scroll it
    layer.resizeWorld();
    
    weapon = game.add.weapon(-1, 'bullet');
     //  The bullet will be automatically killed when it leaves the world bounds
    weapon.bulletKillType = Phaser.Weapon.KILL_WORLD_BOUNDS;
   
    //  The speed at which the bullet is fired
    weapon.bulletSpeed = 500;
    
    //  Speed-up the rate of fire, allowing them to shoot 1 bullet every 60ms
    weapon.fireRate = 0;
    weapon.onFire.add(boom);
     // Set Properties to Tiles
   // Set Properties to Tiles
   var temp = 0;
   for (var y = 0; y < 10; y++)
   {
       for (var x = 0; x < 13; x++)
       {
           let tile = land.getTile(x, y, layer);
           tile.properties.life = (window.localStorage.getItem('data')[temp] == 0) ? 3 : 0;
           //tile.setCollisionCallback(layerHit,this);
           
           temp += 2;
       }
   }
   myTeamGroup = game.add.group();
   myEnemyGroup = game.add.group();
   collectiblesGroup = game.add.group();

    //Get all players connected
    socket.on('currentPlayers', function (players) {
        Object.keys(players).forEach(function (id) {
            if (players[id].playerId === socket.id)
                addMyPlayer(players[id]);
            else
                addTeammates(players[id]);
        });
    });

    //Add a new player
    socket.on('newPlayer', function (playerInfo) {
        addTeammates(playerInfo);
    });

    // If the client dc the player is distroyed.
    socket.on('disconnect', function (playerId) {
        allies.forEach(function (ally) {
          if (playerId === ally.socketId) {
            ally.tank.destroy();
            ally.turret.destroy();
          }
        });
    });

    // Send the our positions changes
    socket.on('playerMoved', function (playerInfo) {
        allies.forEach(function (ally) {
          if (playerInfo.playerId === ally.socketId) {
            ally.tank.position = playerInfo.position;
            ally.tank.rotation = playerInfo.rotation;
            ally.turret.position = playerInfo.position;
            ally.turret.rotation = playerInfo.rotation;
          }
        });
    });

    socket.on('currentCollectibles',function(collectiblesArray){ // [{type: 'speedCollectible',x: 200, y: 200},...]
        for(collectible of collectiblesArray){ //{type: 'speed', x: (emptyTile.x * 64), y: (emptyTile.y * 64), id: i}
            let c1 = new Collectible({x: collectible.x,y: collectible.y},collectible.id,collectible.type,game);
            c1.collectibleSprite.bringToTop();
            collectibles.push(c1);
            collectiblesGroup.add(c1.collectibleSprite);
        }
    });

    socket.on('currentEagles',function(eaglesArray){ 
        for(eagle of eaglesArray){ 
            let e1 = new Eagle({x: eagle.x,y: eagle.y},eagle.id,eagle.type,game);
            e1.eagleSprite.bringToTop();
            collectibles.push(e1);
            collectiblesGroup.add(e1.eagleSprite);
        }
    });

    socket.on('collectibleTaken',function(collectibleId){ //{x: 50, y: 50},1(id),'speed',game);
        let collectibleIndex = collectibles.findIndex((element)=>{return element.collectibleSprite.id == collectibleId;})
        let collectibleSprite = collectibles[collectibleIndex].collectibleSprite;
        delete collectibles[collectibleIndex];
        collectiblesGroup.removeChild(collectibleSprite);
    });

  //  game.input.onDown.add(getTileProperties, this);
     socket.on('update-bullets', function(bullet_server){
        addServerBullet(bullet_server);
    });
    socket.on('update-score', function(score_server){
        updateGameScore(score_server);
    });
    logo = game.add.sprite(0, 200, 'logo');
    logo.fixedToCamera = true;

    game.input.onDown.add(removeLogo, this);

    //game.input.onDown.add(getTileProperties, this);
    cursors = game.input.keyboard.createCursorKeys();
}
function removeLogo () {

    game.input.onDown.remove(removeLogo, this);
    logo.kill();

}

//onFire any bullet, then plays boom sound, to simulate the shot from the turret.
function boom(bullet, weapon){
    explosion.play();
}


function layerHit(bullet, weapon){
    //explosion.play();
}

function render () {
    // game.debug.text('Active Bullets: ' + bullets.countLiving() + ' / ' + bullets.length, 32, 32);
    game.debug.text('RED: ' + game_score.red + ' /  BLUE: ' + game_score.blue, 32, 32, 'yellow');
}

function update () {
    if(myPlayer){ 
      
        if (cursors.left.isDown)
            {currentSpeed = 0;        
            myPlayer.tank.angle -= 2;}
        else if (cursors.right.isDown){
            currentSpeed = 0;
            myPlayer.tank.angle += 2;}
        else if (cursors.up.isDown)
            currentSpeed = 100;
        else
            currentSpeed = 0;
        
        game.physics.arcade.velocityFromRotation(myPlayer.tank.rotation, currentSpeed, myPlayer.tank.body.velocity);
    
        myPlayer.turret.position = myPlayer.tank.position;
        myPlayer.turret.rotation =  myPlayer.tank.rotation;

        // emit player movement
        let newPosition = myPlayer.tank.position;
        let newRotation = myPlayer.tank.rotation;
     
//aquí
        //game.physics.arcade.overlap(weapon, myPlayer.tank, bulletHitPlayer, null, this);

        if (myPlayer.tank.oldPosition && 
           (newPosition !== myPlayer.tank.oldPosition || newRotation !== myPlayer.tank.oldRotation )) {
            socket.emit('playerMovement', {position: newPosition, rotation: newRotation});
        }
        // save old position data
        myPlayer.tank.oldPosition = myPlayer.tank.position;
        myPlayer.tank.oldRotation = myPlayer.tank.rotation;
        
       
        game.physics.arcade.collide(myPlayer.tank,myTeamGroup,function bulletHitPlayer(ally,tank){
             
            currentSpeed = 0;
       
            game.physics.arcade.velocityFromRotation(ally.rotation, currentSpeed, ally.body.velocity);
            game.physics.arcade.velocityFromRotation(tank.rotation, currentSpeed, tank.body.velocity);
        });
        game.physics.arcade.collide(myPlayer.tank,myEnemyGroup,function bulletHitPlayer(enemy,tank){
             
            currentSpeed = 0;
       
            game.physics.arcade.velocityFromRotation(enemy.rotation, currentSpeed, enemy.body.velocity);
            game.physics.arcade.velocityFromRotation(tank.rotation, currentSpeed, tank.body.velocity);
        });
        game.physics.arcade.collide(myTeamGroup,weapon.bullets,function bulletHitLayer(tank,bullet){
            bullet.kill();
            currentSpeed = 0;
            tank.health--;
           // console.log(tank.health);
            if(tank.health == 0){
                player_death.play();
                tank.kill();
                if(tank.team == 'blue'){
                    socket.emit('score-broadcast',{blue : game_score.blue++, red: game_score.red});
                }
            }
            tankHit.play();
            game.physics.arcade.velocityFromRotation(tank.rotation, currentSpeed, tank.body.velocity);
        });
        game.physics.arcade.collide(myEnemyGroup,weapon.bullets,function bulletHitLayer(tank,bullet){
            bullet.kill();
            currentSpeed = 0;
            tank.health--;
           // console.log(tank.health);
            if(tank.health == 0){
                player_death.play();
                tank.kill();
                if(tank.team == 'red'){
                    socket.emit('score-broadcast',{blue : game_score.blue, red: game_score.red++});
                }
            }
            tankHit.play();
            game.physics.arcade.velocityFromRotation(tank.rotation, currentSpeed, tank.body.velocity);
        });
        game.physics.arcade.collide(myTeamGroup, collectiblesGroup, function (tank,collected) {
            speedLimit = 200;
            //socket.emit('bonusCollected',collected); //Error 'too much recursion'
            collected.destroy();
        });

        game.physics.arcade.collide(myPlayer.tank,layer);
        game.physics.arcade.collide(layer,weapon.bullets,function bulletHitLayer(bullet,layer){
            bullet.kill();
            getFiredTile({x:layer.x,y:layer.y});
           
        });
        if (game.input.activePointer.isDown)
        {
            //Boom!     
            fire();
        }
    }
} 


function getFiredTile(tile) {
    let newTile = new Phaser.Tile();

    let earthtile = land.getTile(0, 0, layer);
    
    var tile = land.getTile(tile.x, tile.y, layer);
    
    tile.properties.life--;

    newTile.copy(earthtile);

    currentLife = tile.properties.life;

    if(currentLife==1){
        land.removeTile(tile.x, tile.y, layer).destroy();
        exploded.play();
      land.putTile(newTile,tile.x,tile.y,layer);

    }

    //console.log(currentLife);
   // land.removeTile(x, y, layer).destroy();
    //tile.properties.wibble = true;
}

function bulletHitLayer (tank, bullet) {
    weapon.destroy();
}
function addServerBullet(bullet_server){
    explosion.play();
   allies.forEach(function (ally) {
        if(ally.socketId == bullet_server.id){
            weapon.trackSprite(ally.tank, 0, 0, true);
            weapon.fire();
        }
    });  
}
function updateGameScore(score_server){
    game_score.red = score_server.red;
    game_score.blue = score_server.blue;
}
function addMyPlayer(playerInfo) {
    myPlayer = new TankObject(playerInfo.position,game,socket.id,playerInfo.team);
    myPlayer.tank.bringToTop();
    myPlayer.turret.bringToTop();  
    myPlayer.team = playerInfo.team;
    if(playerInfo.team == 'blue')
        myTeamGroup.add(myPlayer.tank);
    else
        myEnemyGroup.add(myPlayer.tank);
}

function addTeammates(playerInfo) {
    let otherPlayer = new TankObject(playerInfo.position,game,playerInfo.playerId,playerInfo.team);
    otherPlayer.tank.bringToTop();
    otherPlayer.turret.bringToTop();  
    otherPlayer.tank.health = 3;
    allies.push(otherPlayer);
    if(playerInfo.team == 'blue')
        myTeamGroup.add(otherPlayer.tank);
    else
        myEnemyGroup.add(otherPlayer.tank);
}

function getTilePosition(Pixel) {
    return Math.floor(Pixel / 64);
}

function fire () {
    if (game.time.now > fire_timer)
    {     
            fire_timer = game.time.now + FIRE_RATE;
            weapon.trackSprite(myPlayer.turret, 0, 0, true);
            weapon.fire();
            socket.emit('shot-broadcast',{id : myPlayer.socketId, team: myPlayer.team});
    }
}

