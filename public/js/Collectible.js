class Collectible{
    constructor(position,id,type,game){
        this.type = type;
        this.collectibleSprite = game.add.sprite(position.x, position.y, type);
        this.collectibleSprite.id = id;
        game.physics.enable(this.collectibleSprite, Phaser.Physics.ARCADE);        
    }
}